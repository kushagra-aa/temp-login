import React, { setState } from "react";
import "./login-form.css";

export default function LoginForm(props) {
  let loggedin = false;
  var User = {
    email: "admin@admin.com",
    pwd: "admin123",
  };
  let lbtn = document.getElementById("logout");
  lbtn.classList.add("hide");

  function getInfo(e) {
    e.preventDefault();
    var email = document.getElementById("email").value;
    var password = document.getElementById("pwd").value;

    // check is user input matches username and password of a current index of the objPeople array
    if (email == User.email) {
      if (password == User.pwd) {
        loggedin = true;
        lbtn.classList.remove("hide");
        // alert(email + " is logged in!!!");
        // stop the function if this is found to be true
        return;
      } else {
        alert("Incorrect Password");
        return;
      }
    }
    alert("Incorrect Email");
  }
  function Logout() {
    lbtn.classList.add("hide");

    // alert("User Has Logged Out");
  }
  function Forgot() {
    // forgot = true;

    console.log(props.forgot);
  }
  return (
    <div className="container bg-dark text-light">
      <form onSubmit={getInfo}>
        <div className="row mb-3">
          <label htmlFor="email" className="col-sm-2 col-form-label">
            Email:
          </label>
          <input
            type="email"
            name="email"
            placeholder="Email"
            required
            id="email"
          />
        </div>
        <div className="row mb-3">
          <label htmlFor="password" className="col-sm-5 col-form-label">
            Password:
          </label>
          <input
            type="password"
            name="pwd"
            placeholder="Password"
            required
            id="pwd"
          />
        </div>
        <button className="btn btn-primary btn-block">Log In</button>
      </form>
      <button className="btn btn-block forgot" onClick={Forgot}>
        Forgot Password?
      </button>
      <button
        className="btn btn-block btn-outline-danger hide forgot"
        id="logout"
        onClick={Logout}
      >
        Logout
      </button>
    </div>
  );
}
