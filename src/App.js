import "./App.css";
import React, { useState } from "react";
import LoginPage from "./components/Login/login-page-component/login-page";
import Header from "./components/Header-component/header";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <LoginPage></LoginPage>
    </div>
  );
}

export default App;
