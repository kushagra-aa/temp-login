import React from "react";
import "./forgot-page.css";

export default function ForgotPage() {
  let email = "admin@admin.com";
  function getAns(e) {
    e.preventDefault();
    var ans = document.getElementById("ans").value;

    if (ans == email) {
      alert(email + " is logged in!!!");
      return;
    } else alert("incorrect email!");
  }
  return (
    <div>
      <div className="container bg-dark text-light">
        Security Question
        <form onSubmit={getAns}>
          <div class="row mt-3 mb-3 ques">
            <label htmlFor="Question" class="col-sm-2 col-form-label">
              What is your Email?:
            </label>
          </div>
          <div class="row mt-2 mb-3">
            <input
              type="ans"
              name="ans"
              placeholder="Answer"
              required
              id="ans"
            />
          </div>
          <button className="btn btn-primary btn-block">Submit</button>
        </form>
      </div>
    </div>
  );
}
