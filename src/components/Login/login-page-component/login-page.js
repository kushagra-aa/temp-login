import React from "react";
import LoginForm from "../login-form-component/LoginForm";
import ForgotPage from "../forgot-page-component/forgot-page";

export default function LoginPage(forgot) {
  forgot = false;
  return (
    <div>
      {/* {!forgot ? <LoginForm></LoginForm> : <ForgotPage></ForgotPage>} */}
      <LoginForm forgot={forgot} />

      {/* <ForgotPage /> */}
    </div>
  );
}
